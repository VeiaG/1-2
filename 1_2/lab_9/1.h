#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
class zavd{
	public:
		int n;
		zavd(){
			n=0;
		}
		void random();
		void input();
		double serkvad();
		void min();
		void max_mod();
		void print();
		void sort_shell();
	private:
		int a[100];
};
void zavd::print(){
	int i;
	printf("Масив:\n");
	for(i=0;i<n;i++){
		printf("%4d",a[i]);
	}
}
void zavd::max_mod(){
	int i, max;
	max=a[0];
	for(i=0;i<n;i++){
		if(abs(a[i])>max)
			max=abs(a[i]);
	}
	for(i=0;i<n;i++){
		if(a[i]<0)
			a[i]=max;
	}
}
void zavd::min(){
	int i,min,index;
	min=a[0];
	for(i=0;i<n;i++){
		if(a[i]<min){
			min=a[i];
			index=i;
		}
	}
	printf("Мінімальний елемент масиву = %4d\nЙого індекс = %4d",min,index);
}
void zavd::random(){
	int i;
	srand(time(NULL));
	for(i=0;i<n;i++){
		a[i]=rand()%100-50;
	}
}
void zavd::input(){
	int i;
	for(i=0;i<n;i++){
		cin>>a[i];
	}
}
double zavd::serkvad(){
	int i,sum=0,kilk=0;
	double ser;
	for(i=0;i<n;i++){
			if(a[i]%2==0){
				sum+=i;
				kilk++;
			}
	}
	ser=sqrt((double)sum/(double)kilk);
	return ser;
}
void zavd::sort_shell()
{
	int i,d,j;
    for (d = n/2; d >= 1; d /= 2)
        for (i = d; i < n; i++)
            for (j = i; j >= d && a[j-d] < a[j]; j -= d)
                swap(a[j], a[j-d]);
}