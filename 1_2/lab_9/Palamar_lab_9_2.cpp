/*
*варіант : №9
1 Обчислити та вивести кількість двохзначних елементів масиву a15(n,m)
2 Створити та вивести одномірний масив з максимальних від’ємних
елементів кожного рядка матриці v16(n,m)
3 У зафарбованій частині матриці а11(n,n) поміняти місцями
елементи рядків, що містять мінімальний та максимальний
елементи. (половина j)
4 Сформувати вектор з додатних елементів багатовимірної матриці
g11(4,5,2). Вивести вихідну матрицю та сформований вектор
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include "2.h"
using namespace std;
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	zavd a;
	printf("Завдання 4\n");
		a.v4();
	printf("\n");
	printf("Введіть n: ");
	cin>>a.n;
	printf("Введіть m: ");
	cin>>a.m;
	a.random();
	a.print();
	printf("\nЗавдання 1\n");
		printf("Кількість двухзначних елементів = %4d",a.r2());
	printf("\nЗавдання 2\n");
		a.max();
	printf("\nЗавдання 3\n");
		a.v3();
		a.print();
	printf("\n");
	system("pause");
	return 0;
}