/*
*варіант : №9
1 Обчислити та вивести середнє квадратичне індексів парних елементів
масиву b2(n).  
2 Знайти та вивести мінімальний елемент та його індекс у заданому масиві
a5(n).
3 У заданому масиві b1(n) усі від’ємні елементи замінити максимальним по
модулю елементом цього масиву.
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include "1.h"
using namespace std;
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	zavd a;
	printf("Введіть розмір масиву: ");
	cin>>a.n;
	a.random();
	//a.input();
	a.print();
	printf("\nЗавдання 1\n");
	printf("Середнє квадратичне індексів парних елементів = %lf",a.serkvad());
	printf("\nЗавдання 2\n");
	a.min();
	printf("\nСортування Шелла\n");
	a.sort_shell();
	a.print();
	printf("\nЗавдання 3\n");
	a.max_mod();
	a.print();
	printf("\n");
	system("pause");
	return 0;
}