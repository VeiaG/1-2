/*
*варіант : №9( 9 -25 )
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int *r5;
	int n, i; 
    printf("Введіть розмір масиву = ");
    cin>>n;
    r5=(int *)malloc((n*sizeof(int)));
    if(!r5){
		printf("Помилка при виділенні пам'яті");
		system("pause");
		return 0;
	}
	printf("Вхідний масив r5: \n");
	for (i=0;i<n;i++)
	    {
	    	r5[i]=rand()%100-50;
	    	printf("%4d",r5[i]);
			if(r5[i]%3 == 0 && r5[i] != 0) {
				r5[i]=i;
			}
			
		} 
	
	printf("\nВихідний масив r5: \n");
	for (i=0;i<n;i++)
	    {
			printf("%4d",r5[i]);
		}
	free(r5);
	cout<<endl;
	system("pause");
	return 0;
}