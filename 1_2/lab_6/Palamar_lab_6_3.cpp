/*
*варіант : №14( 9 -45 )
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
void gotoxy(int xpos, int ypos)
{ COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X = xpos; scrn.Y = ypos;
	SetConsoleCursorPosition(hOuput,scrn);
}
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	const int N=4, M=3, H=4;
	int i, j, k, sum=0;
	int *g14;
	g14 = new int[N*M*H];
	for (i = 0; i < N; i++)
	for (j = 0; j < M; j++)
	for (k = 0; k < H; k++)
		{
		*(g14 + i * M * H + j * H + k) = rand()%100-50;
		gotoxy(((j+k)*4+1),(((1+i)*M)-j));
		printf("%d", *(g14 + i * M * H + j * H + k));
			if(*(g14 + i * M * H + j * H + k)>0){
				sum+=*(g14 + i * M * H + j * H + k);
			}
		}
	gotoxy(0, N*M+1);
	printf("Сума додатних елементів = %4d\n",sum);
	delete [] g14;
	system("pause");
	return 0;
}