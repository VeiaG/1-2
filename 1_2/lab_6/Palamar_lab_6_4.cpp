/*
*варіант : №24( 9 -35 )
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int *v93, *a;
	int n,m, i,j; 
    printf("Введіть n = ");
    cin>>n;
    printf("Введіть m = ");
    cin>>m;
    v93=(int *)malloc((n*m*sizeof(int)));
    a=(int *)malloc(((m/2)*sizeof(int)));
    if(!v93 || !a){
		printf("Помилка при виділенні пам'яті");
		system("pause");
		return 0;
	}
	for (j=0;j<(m+1)/2;j++)
		    {
					a[j]=0;
			} 
	printf("Вхідний масив v93: \n");
	for (i=0;i<n;i++){
		for (j=0;j<m;j++)
		    {
		    	*(v93+i*m+j)=rand()%100-50;
				printf("%4d",*(v93+i*m+j));
				if(j%2 == 0 && *(v93+i*m+j)%2 != 0){
					a[j/2]++;
				}
			} 
			printf("\n");
	}
	printf("Вихідний масив : \n");
	for (j=0;j<(m+1)/2;j++)
		{
			printf("%4d    ",a[j]);
		} 
	printf("\n");
	free(v93);
	free(a);
	cout<<endl;
	system("pause");
	return 0;
}
