/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 20.02.22	
*дата останньої зміни 20.02.22
*Лабораторна №3
*завдання :
//1
В заданій матриці G(5,4) визначити середнє геометричне значення додатних
елементів стовпчиків, що мають парні індекси. Вивести середні геометричні
значення, оформлені у вигляді масиву.
//2
. В заданий матриці H(3,8) знайти суму всіх парних елементів. Вивести
вихідну матрицю та суму вказаних елементів.
//3
задана формула
//4
задана формула
*варіант : №9 
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include "1-2.h"
extern double __declspec(dllexport) sum(int, int);
extern double __declspec(dllexport) dob(int, int);
extern void __declspec(dllexport) zavd_4();
using namespace std;
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	//Завдання 1
	printf("Завдання 1:\n");
	int i,j;
	int G[5][4],g[5];
	double e_g[2];
	random2(G[0],5,4);
	printf("\nСформований масив середніх геометричних: \n");
	for(j=0;j<4;j++){
		if(j%2 == 0){
			for(i=0;i<5;i++){
				g[i]=G[i][j];
			}
			e_g[j/2]=ser_g(g,5);
			printf(" %1.2f ",e_g[j/2]);
		}
	}
	cout<<endl;
	system("pause");
	//Завдання 2
	int H[3][8];
	system("cls");
	printf("Завдання 2:\n");
	random2(H[0],3,8);
	parn(H[0],3,8);
	cout<<endl;
	system("pause");
	system("cls");
	//Завдання 3
	printf("Завдання 3:\n");
	double X;
	X=sum(1,90)+dob(1,20);
	printf("X = %f\n",X);
	system("pause");
	//Завдання 4
	system("cls");
	printf("Завдання 4:\n");
	zavd_4();
	////
	system("pause");
	return 0;
}
