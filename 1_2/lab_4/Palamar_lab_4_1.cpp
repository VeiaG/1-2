/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 21.03.22
*дата останньої зміни 21.03.22
*Лабораторна №4
*завдання :
*варіант : №9 
*/
#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
//функція створення файлу та запис матриці
void p1()
{       
         system("cls");
         int i, j, n=0, m=0,x;
         FILE *f;     //f - файлова зміна
         f=fopen("p1.txt", " w+");
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }
         printf("Введіть кількість рядків матриці: ");
         cin>>n;     
         printf("Введіть кількість стовпчиків матриці: ");
         cin>>m;    
         fprintf(f, "n=%d ", n);
         fprintf(f, "m=%d \n", m);
         for(i=0; i<n; i++)
         {
                   for(j=0; j<(m-1); j++)
                   {
                            x=(rand()%50);
                            printf("%4d ", x);
                            fprintf(f, "%4d ", x);
                   }
                   x=rand()%50;
                   printf("%4d \n", x);
                   fprintf(f, "%4d \n", x);
         }
         fclose(f); 
         printf("Створено файл p1\n");
         system("pause");
}
//процедура створення та запис у файл символьного рядка
void p2()
{
         system("cls");
         int i, m;
         FILE *f;               // оголошуємо файлову змінну
         char str[77];
         cin.ignore(1,'\n');      
         f=fopen("p2.txt", "w+");//відкриваємо файл для зчитування в текстовому режимі
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }
         printf("Для припинення введіть порожні рядок\n");
         do
         {
                   printf("%d: ", i);
                   gets(str);
                   if (!*str) break;
                   m=strlen(str);
                   for(i=0;i<m;i++)
                            putc(str[i],f);
                   putc('\0',f);
                   putc('\n',f);
         }
         while(*str);
         putc('\0',f);
         fclose(f);      // закриваємо файл
         printf("Створено файл p2 \n");   
		 system("pause");          
}

//процедура створення та запис у файл матриці по елементно
void p3()
{       
         system("cls");
         int i, j, n=0, m=0,x;
         FILE *f;     //f - файлова зміна
         cin.ignore(1,'\n');     
         f=fopen("p3.dat", " w+b"); //відкриваємо файл для роботи з бінарними даними
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }
         printf("Введіть кількість рядків матриці: ");
         cin>>n;     
         printf("Введіть кількість стовпчиків матриці: ");
         cin>>m;    
         fwrite(&n, sizeof(int), 1, f); // записуємо першим елементом кількість рядків матриці
         fwrite(&m, sizeof(int), 1, f); // записуємо першим елементом кількість стовпчиків
         for(i=0; i<n; i++)
         {
                   for(j=0; j<m; j++)
                   {
                            x=rand()%50;
                            printf("%4d ", x);
                            fwrite(&x, sizeof(int), 1, f);
                   }
                   cout<<endl;
         }
         fclose(f); 
         printf("Створено файл p3 \n");
         system("pause");
}
//процедура створення та запис у файл матриці по рядках
void p4()
{       
         system("cls");
         int i, j, n=0, m=0,x;
         FILE *f;     //f - файлова зміна
         cin.ignore(1,'\n'); 
         f=fopen("p4.dat", " w+b"); //відкриваємо файл для роботи з бінарними даними
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }
         printf("Введіть кількість рядків матриці: ");
         cin>>n;     
         printf("Введіть кількість стовпчиків матриці: ");
         cin>>m;    
         int v[m];
         fwrite(&n, sizeof(int), 1, f); // записуємо першим елементом кількість рядків матриці
         fwrite(&m, sizeof(int), 1, f); // записуємо першим елементом кількість стовпчиків
         for(i=0; i<n; i++)
         {
                   for(j=0; j<m; j++)
                   {
                            x=rand()%50;
                            v[j]=x;
                            printf("%4d ", x);
                   }
                   fwrite(&v, sizeof(v), 1, f);
                   cout<<endl;
         }
         fclose(f); 
         printf("Створено файл p4 \n");
} 
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	p1();
	p2();
	p3();
	p4();
	system("pause");
	return 0;
}