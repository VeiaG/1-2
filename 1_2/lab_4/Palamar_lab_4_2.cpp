#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
using namespace std;    
int main()
{       
         system("cls");
         SetConsoleCP(1251);
         SetConsoleOutputCP(1251);
         int i, j, n=0, m=0,x;
         FILE *f1, *f2;      //f1 - вхідний файл, *f2 - вихідний файл
         char sf1[40], sf2[40];// змінна для повного імені файлу
         printf("Введіть ім'я вхідного файлу: ");
         gets(sf1);
         f1=fopen(sf1, "r+");//відкриваємо файл для зчитування в текстовому режимі
         if(f1 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f1, "n=%d ", &n); // зчитуємо кількість рядків матриці
         fscanf(f1, "m=%d ", &m); // зчитуємо кількість стовпчиків матриці
         int a[n][m];  
         int r[m];
         	for(i=0;i<n;i++)
                   for(j=0;j<m;j++)
                   {
                            fscanf(f1, "%d ", &x);     //зчитуємо елементи матриці
                            a[i][j]=x;
                   }
         for (j = 0; j < m; j++)
         {
         		   r[j]=0;
                   for (i = 0; i < n; i++)
                   {
                   		if(i%2==0)
                            r[j] = r[j] + a[i][j];
                    }
        }
         // виведення на екран матриці та результатів обчислення
         printf("Матриця a[%d,%d]:\n",n,m);
         for(i=0;i<n;i++)
         {
                   for(j=0;j<m;j++)
                            printf("%4d ",a[i][j]);
                   printf("\n");

         }
         printf("\n Масив r(%d): \n",m);
         for (j = 0; j < m; j++)
                   printf("%4d ",r[j]);
         printf("\n");
         printf("\nВведіть ім'я файлу для результату: ");
         gets(sf2);    
         f2=freopen(sf2, "w+", stdout); //відкриваємо та перенаправляє потік виведення у заданий файл
         //якщо вдалось відкрити та перенаправити потік виведення усе виведення буде записуватись у заданий файл і не виводитись на екран
         if(f2== NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         printf("Матриця a[%d,%d]:\n",n,m);
         for(i=0;i<n;i++)
         {
                   for(j=0;j<m;j++)
                            printf("%4d ",a[i][j]);
                   printf("\n");
         }
         printf("\n Масив r(%d): \n",m);
         for (j = 0; j < m; j++)
                   printf("%4d ",r[j]);
         printf("\n");
         fclose(f2); 
         fclose(f1); 
         return 0;
}