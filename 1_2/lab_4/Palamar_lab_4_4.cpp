#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
using namespace std;    
int main()
{
         system("cls");
         SetConsoleCP(1251);
         SetConsoleOutputCP(1251); 
         FILE *f1, *f2; // оголошуємо файлову змінну
         char sf1[40], sf2[40]; // змінна для повного імені файлу
         printf("Введіть ім'я вхідного файлу: ");
         gets(sf1); 
         f1=fopen(sf1, "r+b");//відкриваємо файл для зчитування в бінарному режимі        
         if(f1 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         printf("Введіть ім'я файлу для результату : ");
         gets(sf2); 
         f2=freopen(sf2, "w+", stdout); //відкриваємо та перенаправляє потік виведення у заданий файл
         if(f2 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         int i, j, n=0, m=0,x, max;
         fread(&n, sizeof(int), 1, f1);
         fread(&m, sizeof(int), 1, f1);
         int a[n][m];  
         for(i=0; i<n; i++)
                for(j=0; j<m; j++)
                   {
                            fread(&x, sizeof(float), 1, f1);
                            a[i][j]=x;
                   }
        max= a[0][0];
        for(i=0; i<n; i++)
                for(j=0; j<m; j++)
                   {
                        if(max < a[i][j])
                            max= a[i][j];
                   }
        ////////
	for (j = 0; j < m; j++){
		for (i = j; i < n; i++){
			if ( ( (i+j)/2 ) < ( ( m+n ) /4 ) )
			a[i][j]=max; 
		}
 	}
        //////
         printf("Матриця a[%d,%d]:\n",n,m);
         for(i=0; i<n; i++)
         {
                   for(j=0; j<m; j++)
                            printf("%4d ",a[i][j]);
                   printf("\n");
         }
         printf("\n");
         fclose(f2);
         fclose(f1);
         return 0;
}       