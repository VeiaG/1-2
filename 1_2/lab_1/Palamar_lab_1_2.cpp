/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 17.01.22
*дата останньої зміни 17.01.22
*Лабораторна №1
*завдання : 
*варіант : №9 
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я , яка аналізує слово , на кількість унікальних символів
int analyze(string str){
	int kilk=0,v=0;
	string tmp;
	cout<<"\n Слово : "<<str<<endl;
	while(0<str.length()){
		tmp = str[0];
		kilk++;
		v=0;
		while(v<str.length()){
			if(tmp[0]==str[v]) {
				str.erase(v,1);
				v=0;
			}
			else{
				v++;
			}
		}
	}
	printf("Кількість = %4d \n ",kilk);
	return kilk;
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string s,a;
	int i=0 ,n=0 , k=0, j=0, l;
	printf("Введіть текст: \n");
    getline(cin,s);
    l =s.length();
    //видалення лишніх пробілів
    while (i<l){
    	if(s[i] == ' '&& s[i+1] == ' ' || s[i]== ' ' && i == l-1 || s[i]== ' ' && i == 0) {
    		s.erase(i,1);
    		l =s.length();
    		i=0;
    	} 
		else { 
    		i++;	
    	}
    } 
    s.append(" ");
    i=0;
     while (i<l){
    	if (s[i]== ' ' || i == l-1){
    			k++;
    			a.append(s,0,n);
    			analyze(a);
    			a.clear();
			s.erase(0,n+1);
    		l =s.length();
    		i=0;
    		n=0;
    	}
    	else{
    		n++;
    		i++;
    	}
    }
	system("pause");
	return 0;
}
