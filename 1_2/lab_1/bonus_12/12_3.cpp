/* 
*варіант : №9 (24)
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я , яка заповнює массив випадковими значеннями
void random2(int *a,int n,int m){
	int i,j;
	printf("Вихідний масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			*(a+ i*n + j)=rand()%100-50;
			printf("%4d",*(a+ i*n + j));
			
		}
		cout<<endl;
	}
}
void analyze(int *a,int n,int m,int b[]){
	int i,j;
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			if(j%2 == 0 && *(a+ i*n + j)%2 != 0 ){
				b[j+1/2]++;
			}
		}
	}
	printf("Відповідь :\n");
	for(i=0;i<m+1/2;i++)
	{
		if(i%2== 0)
		printf("%4d    ",b[i]);
	}
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n,m,k;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a[n][m];
	int b[m+1/2];
	random2(a[0],n,m);
	for(k=0; k<m+1/2 ; k++){
		b[k]=0;
	}
	analyze(a[0],n,m,b);
	
	cout<<endl;
	system("pause");
	return 0;
}