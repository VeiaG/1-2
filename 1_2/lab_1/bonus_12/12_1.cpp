/* 
*варіант : №9 (14)
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
double x=2.4;
//ф-я суми
double sum(int i, int n){
	double sum=0;
	for(;i<n;i++){
		sum+=pow(x,i)/i;
	}
	return sum;
}
//ф-я добутку
double dob(int k , int n){
	double dob = 1;
	for(;k<n;k++){
		dob*=(pow(-1,3*k)*pow(x,2*k -2 ))/(2*k - 2);
	}
	return dob;
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double A;
	int i,k,n1,n2;
	printf("Введіть i (початок суми):");
	cin>>i;
	printf("Введіть n1 (кінець суми):");
	cin>>n1;
	printf("Введіть k (початок добутку):");
	cin>>k;
	printf("Введіть n2 (кінець добутку):");
	cin>>n2;
	A = pow(x,cos(x))*sum(i,n1)+dob(k,n2)/asin(0.65);
	printf("A = %f \n",A);
	system("pause");
	return 0;
}