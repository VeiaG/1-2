/* 
*варіант : №9 (12)
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я , яка заповнює массив випадковими значеннями
void random(int mas[],int size){
	int i;
	printf("Вихідний масив :");
	for(i=0 ; i<size ; i++){
		mas[i]=rand()%100-50;
		printf("%4d",mas[i]);
		
	}
}
void analyze(int mas[],int size,int n){
	int i,kilk=0;
	for(i=0 ; i<size ; i++){
		if(mas[i]%2 != 0 && mas[i]>0)
			kilk++;
	}
	printf("\n Кількість непарних додатніх елементів масиву №%d = %4d ",n,kilk);
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int as,bs,cs;
		printf(" Введіть розмір першого масиву: ");
		cin>>as;
		int a[as];
		random(a,as);
		printf("\n Введіть розмір другого масиву: ");
		cin>>bs;
		int b[bs];
		random(b,bs);
		printf("\n Введіть розмір третього масиву: ");
		cin>>cs;
		int c[cs];
		random(c,cs);
	analyze(a,as,1);
	analyze(b,bs,2);
	analyze(c,cs,3);
	cout<<endl;
	system("pause");
	return 0;
}