/* 
*варіант : №9 (24)
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;

//ф-я , яка заповнює массив випадковими значеннями
void random2(int *a,int n,int m){
	int i,j;
	printf("Вихідний масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			*(a+ i*n + j)=rand()%100-50;
			printf("%4d",*(a+ i*n + j));
			
		}
		cout<<endl;
	}
}
//sum above
int sum_a(int *a, int n , int m){
	int i ,j ,sum=0;
	for(i=0;i<n;i++){
		for(j=i;j<m;j++){
			sum+=*(a+ i*n + j);
		}
	}
	return sum;
}
//sum below
int sum_b(int *a, int n , int m){
	int i ,j ,sum=0;
	for(i=0;i<n;i++){
		for(j=0;j<(1+i);j++){
			sum+=*(a+ i*n + j);
		}
	}
	return sum;
}
void check(int a ,int b){
	if(a == b){
		printf("Сума елементів над головною діагоналлю рівна сумі під \n");
	}
	else{
		printf("Сума елементів над головною діагоналлю не рівна сумі під \n");
	}
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n,m,suma,sumb;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a[n][m];
	random2(a[0],n,m);
	suma=sum_a(a[0],n,m);
	sumb=sum_b(a[0],n,m);
	check(suma,sumb);
	system("pause");
	return 0;
}
