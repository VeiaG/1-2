/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 17.01.22
*дата останньої зміни 17.01.22
*Лабораторна №1
*завдання :
*варіант : №9 
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я , яка перевіряє , чи наявні в заданому масиві елементи кратні 3
void check3(int mas[]){
	int i;
	for(i=0; i<10 ; i++){
			if(mas[i] % 3 == 0){
				printf("\n В заданому масиві наявні елементи кратні 3\n");
				return;
			}
	}
	printf("\n В заданому масиві немає елементів кратних 3\n");
	
}
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int C[10];
	int i;
	printf(" Задайте масив С(10): ");
	for(i=0; i<10 ; i++){
		cin>>C[i];
	}
	printf("\n Сформований масив: ");
	for(i=0; i<10 ; i++){
		printf("%4d",C[i]);
	}
	check3(C);
	system("pause");
	return 0;
}
