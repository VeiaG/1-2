/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 17.01.22
*дата останньої зміни 17.01.22
*Лабораторна №1
*завдання :
*варіант : №9 
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я , яка заповнює массив випадковими значеннями
void random(int mas[],int size){
	int i;
	for(i=0 ; i<size ; i++){
		mas[i]=rand()%100-50;
	}
}
//ф-я , яка знаходить мінімальне значення масиву
int min(int mas[], int size){
	int min,i;
	min = mas[0];
	for(i=0 ; i<size ; i++){
		if(mas[i]<min){
			min=mas[i];
		}
	}
	return min;
}
int main() {
	srand(time(NULL));
	int A[10],B[15],C[20];
	int AMIN,BMIN,CMIN;
	double FM1;
	random(A,10);
	random(B,15);
	random(C,20);
	AMIN = min(A,10);
	BMIN = min(B,15);
	CMIN = min(C,20);
	FM1=exp(AMIN)+2.11*(BMIN+CMIN);
	printf(" AMIN = %4d \n BMIN = %4d \n CMIN = %4d \n FM1 = %f \n " ,AMIN,BMIN,CMIN,FM1);
	system("pause");
	return 0;
}
