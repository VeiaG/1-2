#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <math.h>
using namespace std;
void sort(int *a,int n,int m){
	int i,j,k;
	int b[n];
	for(j=0 ; j<m ; j++){
		for(i=0; i<n ; i++){
			b[i]=*(a+ i*n + j);
		}
		k=0;
		int tmp;
		while(k<n-1){
			if (b[k]>b[k+1]){
				tmp=b[k];
				b[k]=b[k+1];
				b[k+1]=tmp;
				k=0;
			}
			else
				k++;
		}
		for(i=0; i<n ; i++){
			*(a+ i*n + j)=b[i];
		}
	}
	printf("\nВихідний масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			printf("%4d",*(a+ i*n + j));
		}
		cout<<endl;
	}
}
int main()
{       
         system("cls");
         SetConsoleCP(1251);
         SetConsoleOutputCP(1251);
         int i, j, n=0, m=0,x;
         FILE *f1, *f2;     
         char sf1[40], sf2[40];
         printf("Введіть ім'я вхідного файлу: ");
         gets(sf1);
         f1=fopen(sf1, "r+");
         if(f1 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f1, "n=%d ", &n); 
         fscanf(f1, "m=%d ", &m); 
         int a[n][m];  
         	for(i=0;i<n;i++)
                   for(j=0;j<m;j++)
                   {
                            fscanf(f1, "%d ", &x);  
                            a[i][j]=x;
                   }
         printf("\n Введіть ім'я файлу для результату: ");
         gets(sf2);    
         f2=freopen(sf2, "w+", stdout); 
         if(f2== NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
		sort(a[0],n,m);
		////
         printf("\n");
         fclose(f1); 
         fclose(f2); 
         return 0;
}