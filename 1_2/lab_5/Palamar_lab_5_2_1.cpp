#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <math.h>
using namespace std;
void ser(int mas[],int size,int name){
	int i,sum=0,kilk=0;
	double serr;
	for(i=0 ; i < size ; i++){
		if(mas[i] < 0 ){	
			sum+=mas[i];
			kilk++;
		}
	}
	serr=(double)sum/(double)kilk;
	printf("Середнє арифметичне відємних елементів масиву F%d (%d): %4f \n", name, size , serr);
}
int main()
{       
         system("cls");
         SetConsoleCP(1251);
         SetConsoleOutputCP(1251);
         int i, j, n=0, x;
         FILE *f1, *f2, *f3, *f4;   
         char sf1[40], sf2[40], sf3[40], sf4[40];
         ////a
         printf("Введіть ім'я вхідного файлу F1: ");
         gets(sf1);
         f1=fopen(sf1, "r+");//відкриваємо файл для зчитування в текстовому режимі
         if(f1 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f1, "n=%d ", &n); // зчитуємо кількість рядків матриці
         int F1[n];  
         	for(i=0;i<n;i++)
                   {
                            fscanf(f1, "%d ", &x);     //зчитуємо елементи матриці
                            F1[i]=x;
                   }
         ///b
         printf("Введіть ім'я вхідного файлу F2: ");
         gets(sf2);
         f2=fopen(sf2, "r+");
         if(f2 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f2, "n=%d ", &n); 
         int F2[n];  
         	for(i=0;i<n;i++)
                   {
                            fscanf(f2, "%d ", &x);  
                            F2[i]=x;
                   }
         ///c
         printf("Введіть ім'я вхідного файлу F3: ");
         gets(sf3);
         f3=fopen(sf3, "r+");
         if(f3 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f3, "n=%d ", &n); 
         int F3[n];  
         	for(i=0;i<n;i++)
                   {
                            fscanf(f3, "%d ", &x);     
                            F3[i]=x;
                   }
         ///
         printf("\n");
         printf("\nВведіть ім'я файлу для результату: ");
         gets(sf4);    
         f4=freopen(sf4, "w+", stdout); //відкриваємо та перенаправляє потік виведення у заданий файл
         //якщо вдалось відкрити та перенаправити потік виведення усе виведення буде записуватись у заданий файл і не виводитись на екран
         if(f4== NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
		////
		ser(F1,12,1);
		ser(F2,14,2);
		ser(F3,16,3);
		////
         printf("\n");
         fclose(f4); 
         fclose(f1); 
         fclose(f2); 
         fclose(f3); 
         return 0;
}