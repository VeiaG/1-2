#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
using namespace std; 
void check3(int mas[]){
	int i;
	for(i=0; i<10 ; i++){
			if(mas[i] % 3 == 0){
				printf("\n В заданому масиві наявні елементи кратні 3\n");
				return;
			}
	}
	printf("\n В заданому масиві немає елементів кратних 3\n");
	
}   
int main()
{       
         system("cls");
         SetConsoleCP(1251);
         SetConsoleOutputCP(1251);
         int i, n=10,x;
         FILE *f1, *f2;      //f1 - вхідний файл, *f2 - вихідний файл
         char sf1[40], sf2[40];// змінна для повного імені файлу
         printf("Введіть ім'я вхідного файлу: ");
         gets(sf1);
         f1=fopen(sf1, "r+");//відкриваємо файл для зчитування в текстовому режимі
         if(f1 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         int a[n];  
        fread(&a, sizeof(a), 1, f1);    //зчитуємо елементи матриці
         printf("\nВведіть ім'я файлу для результату: ");
         gets(sf2);    
         f2=freopen(sf2, "w+", stdout);
         if(f2== NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         printf("\n Сформований масив: ");
		for(i=0; i<n ; i++){
			printf(" %4d ",a[i]);
		}
		check3(a);
         printf("\n");
         fclose(f2); 
         fclose(f1); 
         return 0;
}