#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <math.h>
#include <float.h>
using namespace std;
void input(double *a,int n,int m,int name){
	int i,j;
	printf("Масив №%d:\n",name);
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			printf("%0.2f ",*(a+ i*n + j));
		}
		cout<<endl;
	}
}
void check(double *a,int n,int m,int name){
		int i,n1,n2,m2;
		double min1,min2;
		min1= DBL_MAX;
		min2= DBL_MAX;
		for(i=0;i<n;i++){
			if(*(a+ i*n + i)<min1){
				min1=*(a+ i*n + i);
				n1=i;
			}
		}
		for(i=0;i<n;i++){
			if(*(a+ i*n + n-i-1)<min2){
				min2=*(a+ i*n + n-i-1);
				n2=i;
				m2=n-i-1;
			}
		}
		printf("Масив №%d:\n",name);
		printf("Мінімальне значення головної діагоналі = %0.2f \n Індекс : %d : %d \n",min1,n1,n1);
		printf("Мінімальне значення побічної діагоналі = %0.2f \n Індекс : %d : %d \n",min2,n2,m2);
}
int main()
{       
         system("cls");
         SetConsoleCP(1251);
         SetConsoleOutputCP(1251);
         int i, j, n=0, m=0;
         double x;
         FILE *f1, *f2, *f3;     
         char sf1[40], sf2[40] ,sf3[40];
         printf("Введіть ім'я вхідного файлу G: ");
         gets(sf1);
         f1=fopen(sf1, "r+");
         if(f1 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f1, "n=%d ", &n); 
         fscanf(f1, "m=%d ", &m); 
         double G[n][m];  
         	for(i=0;i<n;i++)
                   for(j=0;j<m;j++)
                   {
                            fscanf(f1, "%lf ", &x);  
                            G[i][j]=x;
                   }
         ////
         printf("Введіть ім'я вхідного файлу W: ");
         gets(sf3);
         f3=fopen(sf3, "r+");
         if(f3 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f3, "n=%d ", &n); 
         fscanf(f3, "m=%d ", &m); 
         double W[n][m];  
         	for(i=0;i<n;i++)
                   for(j=0;j<m;j++)
                   {
                            fscanf(f3, "%lf ", &x);  
                            W[i][j]=x;
                   }
		 ///
		 printf("\n Введіть ім'я файлу для результату: ");
         gets(sf2);    
         f2=freopen(sf2, "w+", stdout); 
         if(f2== NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
        input(G[0],5,5,1);
		input(W[0],4,4,2);
		check(G[0],5,5,1);
		check(W[0],4,4,2);
		////
         printf("\n");
         fclose(f1);
		 fclose(f3);  
         fclose(f2); 
         return 0;
}