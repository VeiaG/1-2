#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <math.h>
using namespace std;
int min(int mas[], int size){
	int min,i;
	min = mas[0];
	for(i=0 ; i<size ; i++){
		if(mas[i]<min){
			min=mas[i];
		}
	}
	return min;
}
int main()
{       
         system("cls");
         SetConsoleCP(1251);
         SetConsoleOutputCP(1251);
         int i, j, n=0, x;
         FILE *f1, *f2, *f3, *f4;   
         char sf1[40], sf2[40], sf3[40], sf4[40];
         ////a
         printf("Введіть ім'я вхідного файлу A: ");
         gets(sf1);
         f1=fopen(sf1, "r+");//відкриваємо файл для зчитування в текстовому режимі
         if(f1 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f1, "n=%d ", &n); // зчитуємо кількість рядків матриці
         int A[n];  
         	for(i=0;i<n;i++)
                   {
                            fscanf(f1, "%d ", &x);     //зчитуємо елементи матриці
                            A[i]=x;
                   }
         ///b
         printf("Введіть ім'я вхідного файлу B: ");
         gets(sf2);
         f2=fopen(sf2, "r+");
         if(f2 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f2, "n=%d ", &n); 
         int B[n];  
         	for(i=0;i<n;i++)
                   {
                            fscanf(f2, "%d ", &x);  
                            B[i]=x;
                   }
         ///c
         printf("Введіть ім'я вхідного файлу C: ");
         gets(sf3);
         f3=fopen(sf3, "r+");
         if(f3 == NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
         fscanf(f3, "n=%d ", &n); 
         int C[n];  
         	for(i=0;i<n;i++)
                   {
                            fscanf(f3, "%d ", &x);     
                            C[i]=x;
                   }
         ///
         printf("\n");
         printf("\nВведіть ім'я файлу для результату: ");
         gets(sf4);    
         f4=freopen(sf4, "w+", stdout); //відкриваємо та перенаправляє потік виведення у заданий файл
         //якщо вдалось відкрити та перенаправити потік виведення усе виведення буде записуватись у заданий файл і не виводитись на екран
         if(f4== NULL)
         {
                   printf("Виникла помилка при відкритті файлу \n");
                   return 0;
         }
		int AMIN,BMIN,CMIN;
		double FM1;
		AMIN = min(A,10);
		BMIN = min(B,15);
		CMIN = min(C,20);
		FM1=exp(AMIN)+2.11*(BMIN+CMIN);
		printf(" AMIN = %4d \n BMIN = %4d \n CMIN = %4d \n FM1 = %f \n " ,AMIN,BMIN,CMIN,FM1);
		////
         printf("\n");
         fclose(f4); 
         fclose(f1); 
         fclose(f2); 
         fclose(f3); 
         return 0;
}