#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
//ф-я генерації випадкового дійсних числа
double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}
void p1_1(char name[] ,int n)
{       
         system("cls");
         int i, j,x;
         FILE *f;     //f - файлова зміна
         f=fopen(name, " w+");	
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }   
         fprintf(f, "n=%d \n", n);
         for(i=0; i<n; i++)
         {
                            x=(rand()%100-50);
                            fprintf(f, "%4d ", x);
         }
         fclose(f); 
         printf("Створено файл \n");
}
//процедура створення та запис у файл символьного рядка
void p1_2()
{
         system("cls");
         int i, m;
         FILE *f;               // оголошуємо файлову змінну
         char s[77];     
         f=fopen("1_2.txt", "w+");//відкриваємо файл для зчитування в текстовому режимі
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }
         printf("Введіть текст: \n");
    	 gets(s);
         m=strlen(s);
            for(i=0;i<m;i++)
            	putc(s[i],f);
         fclose(f);      // закриваємо файл
         printf("Створено файл 1_2.txt \n");   
		 system("pause");          
}

//процедура створення та запис у файл матриці по елементно
void p1_3()
{       
         system("cls");
         int i, n=10,x;
         int a[n];
         FILE *f;     //f - файлова зміна   
         f=fopen("1_3.dat", " w+b"); //відкриваємо файл для роботи з бінарними даними
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }  
         for(i=0; i<n; i++)
         {
                            a[i]=rand()%50;
         }
         fwrite(&a, sizeof(a), 1, f);
         fclose(f); 
         printf("\n Створено файл 1_3.dat \n");
         system("pause");
}
void p2_2()
{       
         system("cls");
         int i, j, n=0, m=0,x;
         FILE *f;     //f - файлова зміна
         f=fopen("2_2.txt", " w+");
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }
         printf("Введіть кількість рядків матриці: ");
         cin>>n;     
         printf("Введіть кількість стовпчиків матриці: ");
         cin>>m;    
         fprintf(f, "n=%d ", n);
         fprintf(f, "m=%d \n", m);
         for(i=0; i<n; i++)
         {
                   for(j=0; j<(m-1); j++)
                   {
                            x=(rand()%100-50);
                            printf("%4d ", x);
                            fprintf(f, "%4d ", x);
                   }
                   x=rand()%50;
                   printf("%4d \n", x);
                   fprintf(f, "%4d \n", x);
         }
         fclose(f); 
         printf("Створено файл\n");
         system("pause");
}
void p2_3(char name[] ,int n , int m)
{       
         system("cls");
         int i, j;
         double x;
         FILE *f;     //f - файлова зміна
         f=fopen(name, " w+");
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         }  
         fprintf(f, "n=%d ", n);
         fprintf(f, "m=%d \n", m);
         for(i=0; i<n; i++)
         {
                   for(j=0; j<(m-1); j++)
                   {
                            x=fRand(-2.0,2.0);
                            printf("%2f ", x);
                            fprintf(f, "%2f ", x);
                   }
                   x=fRand(-2.0,2.0);
                   printf("%2f \n", x);
                   fprintf(f, "%2f \n", x);
         }
         fclose(f); 
         printf("Створено файл\n");
         system("pause");
}
void p3(char name[] ,int n , int m)
{       
         system("cls");
         int i, j,x;
         FILE *f;     //f - файлова зміна
         f=fopen(name, " w+");
         if(f == NULL)
         {
                   printf("Виникла помилка при створенні файлу \n");
         } 
         fprintf(f, "n=%d ", n);
         fprintf(f, "m=%d \n", m);
         for(i=0; i<n; i++)
         {
                   for(j=0; j<(m-1); j++)
                   {
                            x=(rand()%100-50);
                            printf("%4d ", x);
                            fprintf(f, "%4d ", x);
                   }
                   x=rand()%100-50;
                   printf("%4d \n", x);
                   fprintf(f, "%4d \n", x);
         }
         fclose(f); 
         system("pause");
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	////1
	p1_1("1_A.txt",10);
	p1_1("1_B.txt",15);
	p1_1("1_C.txt",20);
	p1_2();
	p1_3();
	/////2
	p1_1("2_F1.txt",12);
	p1_1("2_F2.txt",14);
	p1_1("2_F3.txt",16);
	p2_2();
	p2_3("2_G.txt",5,5);
	p2_3("2_W.txt",4,4);
	/////3
	p3("3_G.txt",5,4);
	p3("3_H.txt",3,8);
	////
	system("pause");
	return 0;
}