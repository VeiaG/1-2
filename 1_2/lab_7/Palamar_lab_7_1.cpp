/*
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <stack>
using namespace std;
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n,i,j,min=50,max=-50,k=0;
	printf("Введіть n (розмір масиву): ");
	cin>>n;
	stack <int> r9;
	printf("Вхідний стек: \n");
	for(i=0;i<n;i++){
		r9.push(rand()%10-5);
		printf("%4d",r9.top());
		if(r9.top()<min){
			min=r9.top();
		}
		if(r9.top()>max){
			max=r9.top();
		}
	}
	if(r9.empty()){
		printf("Стек пустий");
		system("pause");
		return 0;
	}
	stack <int> temp;
	for(i=0;i<n;i++){
		if(r9.top()!=0) {
			temp.push(min+max);
			k++;
		}
		else {
			temp.push(r9.top());
		}
		r9.pop();
	}
		printf("\nВихідний стек: \n");
	for(i=0;i<n;i++){
		r9.push(temp.top());
		temp.pop();
		printf("%4d",r9.top());
	}
	printf("\nКількість замін = %4d\n",k);
	system("pause");
	return 0;
}