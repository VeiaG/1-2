/*
*варіант : №9
Написати функцію, що визначає чи є список упорядкованим за
спаданням.
Двохзв’язаний лінійний список
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <stack>
using namespace std;
class Node
{
    public:
    int data;
    Node* next;
    Node* prev;
};
void push(Node** head_ref, int new_data)
{
    Node* new_node = new Node();
    new_node->data = new_data;
    new_node->next = (*head_ref);
    new_node->prev = NULL;
    if ((*head_ref) != NULL)
        (*head_ref)->prev = new_node;
    (*head_ref) = new_node;
}
void append(Node** head_ref, int new_data)
{
    Node* new_node = new Node();
    Node* last = *head_ref;
    new_node->data = new_data;
    new_node->next = NULL;
    if (*head_ref == NULL)
    {
        new_node->prev = NULL;
        *head_ref = new_node;
        return;
    }
    while (last->next != NULL)
        last = last->next;
    last->next = new_node;
    new_node->prev = last;
    return;
}
void display(Node* node)
{
    Node* last;
    while (node != NULL)
    {
        printf("%4d",node->data);
        last = node;
        node = node->next;
    }
}

void check(Node* node,int n)
{
	int i,k , lt, rt, tmp3;
	int tmp[n];
	int tmp2[n];
    Node* last;
    while (node != NULL)
    {
        tmp[i]=node->data;
        tmp2[i]=node->data;
        last = node;
        node = node->next;
        i++;
    }
    //сортування
			 lt = 0;
			 rt = n-1;
			 while (lt < rt)
			  {
			  for (k = rt; k > lt; k--)
			   if (tmp[k-1] > tmp[k])
			   {
			   tmp3 = tmp[k];
			   tmp[k] = tmp[k - 1];
			   tmp[k - 1] = tmp3;
			   }
			  lt++;
			  for (k = lt; k < rt; k++)
			   if (tmp[k]>tmp[k+1])
			   {
			   tmp3 = tmp[k];
			   tmp[k] = tmp[k + 1];
			   tmp[k + 1] = tmp3;
			   }
			  rt--;
				}
	for(i=0;i<n;i++){
		if(tmp[i]!=tmp2[i])
		{
			printf("\nСписок упорядкований за спаданням");
			return;
		}
	}
	printf("\nСписок не упорядкований за спаданням");
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	 Node* mas = NULL;
	int n=0,i,x;
	printf("Введіть список: \nДля припинення введіть щось окрім цифри...\n");
	do{
		cin>>x;
		if(x!=NULL){
			append(&mas,x);
			n++;
		}
	}
	while(x!=NULL);
	printf("Вхідний список: \n");
	display(mas);
	check(mas,n);
	printf("\n");
	system("pause");
	return 0;
}
