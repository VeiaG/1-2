/*
*варіант : №9
Сформувати масив c5(2n) з елементів вхідного масиву a3(n), розмістивши
на парних місцях елементи масиву a3(n), а на непарних – нулі
Двохзв’язаний кільцевий список
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <stack>
using namespace std;
struct Node
{
    int data;
    struct Node *next;
    struct Node *prev;
};
//вставити в кінці списку
void push_e(struct Node** start, int value)
{
    if (*start == NULL)
    {
        struct Node* new_node = new Node;
        new_node->data = value;
        new_node->next = new_node->prev = new_node;
        *start = new_node;
        return;
    }
    Node *last = (*start)->prev;
    struct Node* new_node = new Node;
    new_node->data = value;
    new_node->next = *start;
    (*start)->prev = new_node;
    new_node->prev = last;
    last->next = new_node;
}
//вставити спочатку списку
void push_b(struct Node** start, int value)
{
    struct Node *last = (*start)->prev;
    struct Node* new_node = new Node;
    new_node->data = value;
    new_node->next = *start;
    new_node->prev = last;
    last->next = (*start)->prev = new_node;
    *start = new_node;
}
void display(struct Node* start)
{
    struct Node *temp = start;
    while (temp->next != start)
    {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("%d ", temp->data);
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	struct Node* a3 = NULL;
	struct Node* c5 = NULL;
	int n,i;
	printf("Введіть n (розмір масиву): ");
	cin>>n;
	printf("Массив а3: \n");
	for(i=0;i<n;i++){
		push_e(&a3,rand()%100-50);
	}
	display(a3); 
	//
	struct Node *temp = a3;
    while (temp->next != a3)
    {
       	push_e(&c5,0);
       	push_e(&c5,temp->data);
        temp = temp->next;
    }
    push_e(&c5,0);
    push_e(&c5,temp->data);
	//
	printf("\nМассив c5: \n");
	display(c5); 
	printf("\n");
	system("pause");
	return 0;
}