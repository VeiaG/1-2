/*
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <graphics.h>
void s(){
	setlinestyle(0,0,1);
}
void l(){
	setlinestyle(3,0,1);
}
int main()
{
	//вершина
	const int xv=200,yv=30;
	initwindow(450,470); // відкриває вікна для графіки разміром 400 на 300
	ellipse(200,200,1,1,100,50);
	line(100,200,xv,yv);
	line(300,200,xv,yv);
	l();
	line(xv,yv,200,200);
	//
	line(200,200,210,151);
	line(210,151,xv,yv);
	//
	l();
	line(200,200,251,242);
	s();
	line(251,242,xv,yv);
	//
	l();
	line(200,200,105,215);
	line(251,242,105,215);
	line(210,151,105,215);
	line(210,151,251,242);
	s();
	line(105,215,xv,yv);
	arc(200,200,-35,80,10);
	arc(200,200,-158,-35,12);
	arc(200,200,-158,-35,10);
	arc(200,200,80,-158,10);
	arc(200,200,80,-158,12);
	arc(200,200,80,-158,14);
	//
	outtextxy(180,125,"b");
	outtextxy(210,210,"r");
	//
	outtextxy(215,185,"A");
	outtextxy(180,180,"B");
	outtextxy(190,210,"C");
	//ця формула точно правильна ) можливо й не коротка , але правильна (теорема синусів і формула герона )
	outtextxy(5,300,"S=sqrt((3r^2-r*cosA-r*cosB-r*cosC)*");
	outtextxy(5,315,"*(r^2+r*cosB-r*cosA-r*cosC)*");
	outtextxy(5,330,"*(r^2+r*cosC-r*cosA-r*cosB))+");
	outtextxy(5,345,"((2*r^2-2*r*cosA)/4)*sqrt(4*(b^2+r^2)^2+");
	outtextxy(5,360,"+2*r^2-2*r*cosA)+((2*r^2-2*r*cosB)/4)*");
	outtextxy(5,375,"*sqrt(4*(b^2+r^2)^2+2*r^2-2*r*cosB)+");
	outtextxy(5,390,"+((2*r^2-2*r*cosC)/4)*sqrt(4*(b^2+r^2)^2+");
	outtextxy(5,405,"+2*r^2-2*r*cosC)");
	getch(); 
	closegraph();
	return 0;
}