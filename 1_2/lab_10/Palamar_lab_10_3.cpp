/*
*варіант : №9
*/

#include <windows.h>
#include <graphics.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <conio.h>
#include <math.h>
#include <stdio.h>
using namespace std;
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
		initwindow(1280,720);
		const int s=100;
		int i,j;
	setgraphmode(1);
	setbkcolor(1);
	setfillstyle(1,9);	
	bar(0,0,1280,720);
	setfillstyle(1,2);
	fillellipse(640,720,700,370);
	setfillstyle(1,14);
	fillellipse(1180,100,50,50);
	setfillstyle(1,6);
	
	
	for(i=0;i<5;i++){
		for(j=0;j<7;j++){
			bar3d(300+50*j,550-50*i,300+50*(j+1),550-50*(i+1),6,1);
			Sleep(s);
		}
	}
	setfillstyle(1,8);
	bar3d(450,550,500,450,0,1);
	setfillstyle(1,7);
	fillellipse(485,515,5,5);
	Sleep(s);
	setfillstyle(4,15);
	bar3d(350,500,400,400,0,1);
	Sleep(s);
	bar3d(550,500,600,400,0,1);
	Sleep(s);
	int poly[6];
	poly[0]=250;
	poly[1]=300;
	poly[2]=700;
	poly[3]=300;
	poly[4]=475;
	poly[5]=190;
	setfillstyle(1,12);
	fillpoly(3,poly);
	getch();
	closegraph();
	return 0;
}