/*
*варіант : №9
*/
#include <windows.h>
#include <graphics.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <conio.h>
#include <math.h>
#include <stdio.h>

using namespace std;
float y(float x)
{
	return 0.5*pow(x,3);
}
int main()
{
	int i ,xg ,yg,xl=0,yl=3200;  
	char *xx=(char *)malloc(20*sizeof(char *)), *yy=(char *)malloc(20*sizeof(char *));
	initwindow(1200,820);
	setgraphmode(1);
	setbkcolor(0);
	setcolor(15);
	 outtextxy(375,415,"0");
	 outtextxy(815,400,"X");
	outtextxy(415,35,"Y");
	moveto(0,400);
	linerel(800,0);
	moveto(400,0);
	linerel(0, 800);
	ellipse(400,400,0,360,5,5);
	line(1000,110,1000,470);
	line(900,150,1100,150);
	outtextxy(950,120, "X");
	outtextxy(1050,120, "Y");
	for(i=0;i<8;i++){
		line(900,190+i*40,1100,190+i*40);
		sprintf(xx,"%d",i-4);
		sprintf(yy,"%7.1f",y(i-4));
		outtextxy(950,160+40*i,xx);
		outtextxy(1010,160+40*i, yy);
	}
	outtextxy(380,775,"-4");
	outtextxy(380,700,"-3");
	outtextxy(380,600,"-2");
	outtextxy(380,500,"-1");
	outtextxy(380,300,"1");
	outtextxy(380,200,"2");
	outtextxy(380,100,"3");
	outtextxy(380,15,"4");
	outtextxy(380,100,"3");
	outtextxy(15,410,"-4");
	outtextxy(100,410,"-3");
	outtextxy(200,410,"-2");
	outtextxy(300,410,"-1");
	outtextxy(500,410,"1");
	outtextxy(600,410,"2");
	outtextxy(700,410,"3");
	outtextxy(775,410,"4");
	for(i=0; i<9; i++){
		if(i!=4){
			line(390,i*100, 410, i*100);
		}
	}
	for(i=0; i<9; i++){
		if(i!=4){
			line(i*100, 410, i*100, 390);
			
		}
	}
	setcolor(2);
	for(i=-40;i<=40;i++){
		xg=200+(i+20)*10;
		yg=400-y((float)i/10)*100;
		line(xl,yl,xg,yg);
		xl=xg;
		yl=yg;
	}
	settextstyle(0, 0, 1);
	outtextxy(470, 150, "y=0.5*x^3");
	///
	setcolor(15);
	//
	setcolor(9);
	getch();
	closegraph();
	return 0;
}
