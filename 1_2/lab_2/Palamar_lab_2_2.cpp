/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 26.01.22
*дата останньої зміни 26.01.22
*Лабораторна №2
*завдання :
*варіант : №9 
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я , яка заповнює массив випадковими значеннями
void random2(int *a,int n,int m){
	int i,j;
	printf("Вхідний масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			*(a+ i*n + j)=rand()%100-50;
			printf("%4d",*(a+ i*n + j));
			
		}
		cout<<endl;
	}
}
//сортування стовбців
void sort(int *a,int n,int m){
	int i,j,k;
	int b[n];
	for(j=0 ; j<m ; j++){
		for(i=0; i<n ; i++){
			b[i]=*(a+ i*n + j);
		}
		k=0;
		int tmp;
		while(k<n-1){
			if (b[k]>b[k+1]){
				tmp=b[k];
				b[k]=b[k+1];
				b[k+1]=tmp;
				k=0;
			}
			else
				k++;
		}
		for(i=0; i<n ; i++){
			*(a+ i*n + j)=b[i];
		}
	}
	printf("\nВихідний масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			printf("%4d",*(a+ i*n + j));
		}
		cout<<endl;
	}
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n,m,k;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a[n][m];
	random2(a[0],n,m);
	sort(a[0],n,m);
	cout<<endl;
	system("pause");
	return 0;
}