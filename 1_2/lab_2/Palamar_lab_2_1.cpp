/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 26.01.22
*дата останньої зміни 26.01.22
*Лабораторна №2
*завдання :
*варіант : №9 
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я для введення значень масиву
void input(int mas[],int size,int name){
	int i;
	printf("\nВведіть масив F%d (%d): ", name, size);
	for(i=0 ; i < size ; i++){
		cin>>mas[i];
	}
	printf("\n Сформований масив F%d (%d): ", name, size);
	for(i=0 ; i < size ; i++){
		printf("%4d",mas[i]);
	}
}
void ser(int mas[],int size,int name){
	int i,sum=0,kilk=0;
	for(i=0 ; i < size ; i++){
		if(mas[i] < 0 ){	
			sum+=mas[i];
			kilk++;
		}
	}
	printf("Середнє арифметичне відємних елементів масиву F%d (%d): %4d \n", name, size , sum/kilk);
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int F1[12],F2[14],F3[16];
	input(F1,12,1);
	input(F2,14,2);
	input(F3,16,3);
	
	system("cls");
	
	ser(F1,12,1);
	ser(F2,14,2);
	ser(F2,16,3);
	
	system("pause");
	return 0;
}
