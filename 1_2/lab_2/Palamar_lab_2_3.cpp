/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 26.01.22
*дата останньої зміни 26.01.22
*Лабораторна №2
*завдання :
*варіант : №9 
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
//ф-я генерації випадкового дійсних числа
double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}
//ф-я , яка заповнює массив випадковими значеннями
void input(double *a,int n,int m,int name){
	int i,j;
	printf("Масив №%d:\n",name);
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			*(a+ i*n + j)=fRand(-2.0,2.0);
			printf("%0.2f ",*(a+ i*n + j));
		}
		cout<<endl;
	}
}
//визначення мінімальних елементів діагоналей та їх індексів
void check(double *a,int n,int m,int name){
		int i,n1,n2,m2;
		double min1,min2;
		min1= DBL_MAX;
		min2= DBL_MAX;
		for(i=0;i<n;i++){
			if(*(a+ i*n + i)<min1){
				min1=*(a+ i*n + i);
				n1=i;
			}
		}
		for(i=0;i<n;i++){
			if(*(a+ i*n + n-i-1)<min2){
				min2=*(a+ i*n + n-i-1);
				n2=i;
				m2=n-i-1;
			}
		}
		printf("Масив №%d:\n",name);
		printf("Мінімальне значення головної діагоналі = %0.2f \n Індекс : %d : %d \n",min1,n1,n1);
		printf("Мінімальне значення побічної діагоналі = %0.2f \n Індекс : %d : %d \n",min2,n2,m2);
}
int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n,m,k;
	double G[5][5],W[4][4];
	input(G[0],5,5,1);
	input(W[0],4,4,2);
	check(G[0],5,5,1);
	check(W[0],4,4,2);
	cout<<endl;
	system("pause");
	return 0;
}
