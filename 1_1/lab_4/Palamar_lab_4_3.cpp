/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 01.11.21 10:18
*дата останньої зміни 01.11.21 23:00
*Лабораторна №4
*завдання :Розробити блок-схему алгоритму для табулювання функції та
реалізувати його мовою С\С++ відповідно варіанту індивідуального завдання
*призначення програмнного файлу: табулювання заданої ф-ї
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double x, y, xstart, xend, xstep;
	cout<<" Введіть інтервал [xstart..xend] \n";
	cout<<" Введіть xstart = ";
	cin>>xstart;
	cout<<" Введіть xend = ";
	cin>>xend;
	cout<<" Введіть xstep = ";
	cin>>xstep;
	printf("_________________________\n");
	printf("| x | y |\n");
	printf("-------------------------\n");
	x=xstart-xstep;
	while (x<=xend)
		{
		x=x+xstep;
		y=(pow(sin(x),3)+pow((x*x)-1, 1/4 ))/3*x;
		printf("| %8.4f | %8.4f |\n", x,y);
		}
	system("pause");
	return 0;
}
