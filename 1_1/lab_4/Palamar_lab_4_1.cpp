/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 01.11.21 10:18
*дата останньої зміни 01.11.21 21:40
*Лабораторна №4 
*завдання :розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для підрахунку суми елементів нескінченного ряду, відповідно
індивідуального завдання
*призначення програмнного файлу: підрахнуок сумми
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main()
 {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n=1 ;
	double x, e, a, sum=0;
	cout<<" Введіть a = ";
	cin>>x;
	a=pow(x-1,n)/n;
	e=pow(10,-4);
	while (a>e)
		{
		n++;
		a=pow(x/n , n);
		sum=sum+a;
		}
	printf(" Сумма = %10.7f Кількість доданків = %d\n",sum,n);
	
	system("pause");
	return 0;
}
