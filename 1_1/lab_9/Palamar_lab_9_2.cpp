/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 02.12.21 
*дата останньої зміни 02.12.21 
*Лабораторна №9
*завдання :
*варіант : №9
*/
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j,k,n,m , lt, rt, tmp;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a[n][m];
	int b[m];
	srand(time(NULL));
	printf("Матриця a( %d, %d): \n",n,m);
		for (i = 0; i < n; i++){
			if (i%2 == 0)
			SetColor(0,4);
			else
			SetColor(0,2);
			for (j = 0; j < m; j++){
				a[i][j] = rand()%100-50;
				printf("%4d", a[i][j]);
			}
		cout<<endl;
		}
	cout<<endl;
	//Сортування
	for(i=0 ; i<n ; i++) {
		if(i%2){
			for(j=0;j<m;j++){
				b[j]=a[i][j];
			}
			 lt = 0;
			 rt = m-1;
			 while (lt < rt)
			  {
			  for (k = rt; k > lt; k--)
			   if (b[k-1] > b[k])
			   {
			   tmp = b[k];
			   b[k] = b[k - 1];
			   b[k - 1] = tmp;
			   }
			  lt++;
			  for (k = lt; k < rt; k++)
			   if (b[k]>b[k+1])
			   {
			   tmp = b[k];
			   b[k] = b[k + 1];
			   b[k + 1] = tmp;
			   }
			  rt--;
			 }
			 
			for(j=0;j<m;j++){
				a[i][j]=b[j];
			}
		}
	}
	SetColor(7,0);
	printf("Відсортована матриця a( %d, %d): \n",n,m);
	for (i = 0; i < n; i++)	{
		if (i%2 == 0)
			SetColor(0,4);
			else
			SetColor(0,2);
		for (j = 0; j < m; j++){
			printf("%4d", a[i][j]);
			}
		cout<<endl;
	}
	SetColor(7,0);
	cout<<endl;
	system("pause");
	return 0;
}
