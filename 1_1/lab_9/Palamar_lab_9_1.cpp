/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 02.12.21 
*дата останньої зміни 06.12.21 
*Лабораторна №9
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для сортування елементів одновимірного числового масиву відповідно
індивідуального завдання, що наведено у таблиці К.1 додатку К.
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j=0,k,n, tmp;
	srand(time(NULL));
	cout<<"Введіть розмір масиву (n) = ";
	cin>>n;
	int a[n];
	printf("Матриця a( %d): \n",n);
	for (i = 0; i < n; i++){
			a[i] = rand()%100 - 50;
			if(a[i]>0 && i%2 != 0) {
				j++;
			}
		printf("%4d", a[i]);
	}
	int b[j];
	k=0;
	for (i = 0; i < n; i++){
			if(a[i]>0 && i%2 != 0) {
				b[k]=a[i];
				k++;
			}
	}
	i=0;
	while(i<j-1){
			if (b[i]<b[i+1]){
				tmp=b[i];
				b[i]=b[i+1];
				b[i+1]=tmp;
				i=0;
			}
			else
			i++;
	}
	k=0;
	printf("\n Відсортована Матриця a( %d): \n",n);
	for (i = 0; i < n; i++){
			if (i%2 != 0 ){
				SetColor(0,5);
					if (a[i]<=0){
					SetColor(0,4);	
					}
			}
			else {
				SetColor(0,6);	
			}
		if(a[i]>0 && i%2 !=0) {
				a[i]=b[k];
				k++;
			}
		printf("%4d", a[i]);
		SetColor(7,0);
	}
	cout<<endl;
	system("pause");

	return 0;
}
