/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 02.12.21 
*дата останньої зміни 02.12.21 
*Лабораторна №9
*завдання :
*варіант : №9
*/
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j,n,m,k, tmp;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int b[n][m];
	int a[m*n];
	srand(time(NULL));
	printf("Матриця a( %d, %d): \n",n,m);
		for (i = 0; i < n; i++){
			for (j = 0; j < m; j++){
				b[i][j] = rand()%100-50;
				a[m*i+j]= b[i][j];
				printf("%4d", b[i][j]);
			}
		cout<<endl;
		}
	//Сортування
	for (k = 0; k < n*m; k++){
		if (k % 2 == 0)
			for (j = n*m - 1; j > 0; j -= 2){
				if (a[j] < a[j - 1]){
					tmp = a[j];
					a[j] = a[j - 1];
					a[j - 1] = tmp;
				}
			}
		else
			for (j = n*m - 2; j > 0; j -= 2){
				if (a[j] < a[j-1]){
					tmp = a[j];
					a[j] = a[j - 1];
					a[j - 1] = tmp;
				}
			}
	}
	
	  int r,s=-1,pi,pj;
  for (r = 0; r < (n+m-1); r++)
    if (r%2!=0)
    {
      if (r<m){
        pi=0;
        pj=m-r-1;
        }
      else{
        pi=r-(m-1);
        pj=0;
        }
	    for (; (pi<n)&(pj<m);pi++,pj++) {
	      s++;
	      b[pi][pj]=a[s];
    	}
    }
    else{
        if (r<n){
        pi=r;
        pj=m-1;
        }
        else{
        pi=n-1;
        pj=m-(r-n+2);
        }
    for (; (0<=pi)&(0<=pj);pi--,pj--){
    s++;
      b[pi][pj]=a[s];
    }
    }
	printf("Відсортована матриця a( %d, %d): \n",n,m);
	for (i = 0; i < n; i++)	{
		for (j = 0; j < m; j++){
			printf("%4d", b[i][j]);
			}
		cout<<endl;
	}
	cout<<endl;
	system("pause");
	return 0;
}
