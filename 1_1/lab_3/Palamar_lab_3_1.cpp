/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 25.10.21 11:46
*дата останньої зміни :25.10.21 11:56
*Лабораторна №3
*завдання :Розробити блок-схему алгоритму табулювання функції y на
заданому проміжку [xstart…xend] та реалізувати його мовою С\С++ відповідно
варіанту індивідуального завдання
*призначення програмнного файлу: табулювання ф-ї в заданому інтервалі із заданим кроком
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double xstart,xend,xstep, x,y;
	cout<<" Введіть інтервал xstart..xend \n";
	cout<<" Введіть xstart = ";	
	cin>>xstart;
	cout<<" Введіть xend = ";
	cin>>xend;
	cout<<" Введіть xstep = ";
	cin>>xstep;
	printf("_________________________\n");
	printf("| x | y |\n");
	printf("-------------------------\n");
	for (x=xstart; x<=xend; x=x+xstep){
		if(x<0){
			y=(pow(cos(x),2)+fabs(x))/(2.5*x);
		}
		else{
			y=sqrt(0.79+pow(x,3));
		}
		printf("| %8.4f | %8.4f |\n", x,y); 
	}
	system("pause");
	return 0;
}
