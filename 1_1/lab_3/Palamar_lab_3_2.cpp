/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 25.10.21 11:46
*дата останньої зміни :25.10.21 12:18
*Лабораторна №3
*завдання : Розробити блок-схему алгоритму для підрахунку сум та добутків,
відповідно індивідуального завдання
*призначення програмнного файлу: обчислення заданої суми
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double sum,fact, n, n_start, n_end;
	cout<<" Введіть початкові значення n = ";
	cin>>n_start;
	cout<<" Введіть кінцеве значення n = ";
	cin>>n_end;
	for(n=n_start; n<=n_end;n++){
			fact=1;
			for(int i=1;i<=n+1;i++){
				fact=fact*i;
			}
			sum=sum+pow(5,n-1)/fact;
	}
	cout<<sum<<endl;
	system("pause");
	return 0;
}
