/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 25.10.21 11:46
*дата останньої зміни :01.11.21 00:44
*Лабораторна №3
*завдання : Розробити блок-схему алгоритму для виконання індивідуального
завдання (Знайти добуток усіх парних чисел в заданому діапазоні. Вивести усі
числа, що задовольняють вказаній умові, а також добуток та кількість
таких чисел)
*призначення програмнного файлу: Знаходження добутку парних чисел на заданому діапазоні.
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	long long int i ,i_start,i_end, n, dob;
	cout<<"Введіть початок діапазону = ";
	cin>>i_start;
	cout<<"Введіть кінець діапазону = ";
	cin>>i_end;
	dob=1;
	n=0;
	for(i=i_start;i<=i_end;i++){
		 if (i % 2 == 0)
   		 {
   		 	cout<<i<<endl;
			dob*=i;
    	    n++;
   		 }
	}
	cout<<"Добуток = "<<dob<<endl;
	cout<<"Кількість = "<<n<<endl;
	
	system("pause");
	return 0;
}
