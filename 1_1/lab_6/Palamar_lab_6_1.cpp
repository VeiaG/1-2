/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 07.11.21 
*дата останньої зміни 07.11.21 19:54
*Лабораторна №6
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для розв‘язання індивідуального завдання
*призначення програмнного файлу: Обчислити та вивести середнє квадратичне парних від‘ємних елементів
масиву a9(n)
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int size, i, sum ,n=0; 
    cout << "Введіть розмір масиву = ";
    cin >> size;
    srand(time(NULL));
    int a9[size];
    cout<<" Сформований масив A["<<size<<"] = ";
  	for (i=0;i<size;i++)
	    {
		a9[i] = rand()%100-50;
		cout<<a9[i]<<" ";
		if (a9[i] % 2 == 0 && a9[i]<0){
			sum += pow(a9[i],2);
			n++;
			}
		} 
	cout<<endl<<"Середнє квадратичне = "<<sqrt(sum/n)<<endl;
	system("pause");

	return 0;
}
