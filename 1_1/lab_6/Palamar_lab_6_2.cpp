/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 07.11.21 
*дата останньої зміни 07.11.21 22:41
*Лабораторна №6
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для пошуку екстремальних значень елементів одновимірного масиву
відповідно індивідуального завдання
*призначення програмнного файлу: У заданому масиві a8(n) знайти та вивести індекси елементів, що більше або дорівнюють заданому числу.
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int size, i,a; 
    cout << "Введіть розмір масиву = ";
    cin >> size;
    cout << "Введіть задане число a = ";
    cin >> a;
    srand(time(NULL));
    int a9[size];
    cout << "Введіть масив А["<<size<<"] = ";
  	for (i=0;i<size;i++)
	    {
			cin>>a9[i];
		} 
	system("cls");
	cout<<" Сформований масив A["<<size<<"] = ";
	for (i=0;i<size;i++)
	    {
			cout<<a9[i]<<" ";
			
		} 
	cout<<endl<<" Індекси чисел масиву , більших за "<<a<<" = ";
	for (i=0;i<size;i++)
	    {
			if (a9[i] >= a){
				cout<<i<<" ";
			}
		}
	system("pause");
	return 0;
}
