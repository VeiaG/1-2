/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 07.11.21 
*дата останньої зміни 07.11.21 22:41
*Лабораторна №6
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для модифікації елементів одновимірного масиву відповідно
індивідуального завдання
*призначення програмнного файлу: У заданому масиві r5(n) усі елементи, що кратні п‘яти, замінити їх
індексами.
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int size, i; 
    cout << "Введіть розмір масиву = ";
    cin >> size;
    srand(time(NULL));
    int r5[size];
    cout << "Введіть масив r5["<<size<<"] = ";
  	for (i=0;i<size;i++)
	    {
			cin>>r5[i];
		} 
	system("cls");
	cout<<" Вхідний масив r5["<<size<<"] = ";
	for (i=0;i<size;i++)
	    {
	    	cout<<r5[i]<<" ";
			if(r5[i]%3 == 0 && r5[i] != 0) {
				r5[i]=i;
			}
			
		} 
	
	cout<<endl<<" Вихідний масив r5["<<size<<"] = ";
	for (i=0;i<size;i++)
	    {
			cout<<r5[i]<<" ";
		}
	system("pause");
	return 0;
}
