/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 28.11.21 
*дата останньої зміни 28.11.21 14:14 
*Лабораторна №8
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для обробки елементів одновимірного масиву відповідно індивідуального
завдання, що надане у таблиці И.1 з додатку И
*варіант : №9 (5)
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,n , k=0;
	srand(time(NULL));
	cout<<"Задайте розмір матриці n = ";
	cin>>n;
	int a4[n];
	int *p; 
	p=&a4[0];
	cout<<"\n Матриця a4(n) = ";
	for (i=0;i<n;i++){
		 *p = rand()%100-40;
		 cout<<*p<<" ";
		 p++;
		}
	p=&a4[0];
	cout<<"\n Парні додатні елементи a4(n) = ";
	for (i=0;i<n;i++){
		 if(*p%2 == 0 && *p>0) {
		 	cout<<*p<<" ";
		 	k++;
		 }
		 p++;
		}
	cout<<"\n Кількість = "<<k<<endl;
	system("pause");

	return 0;
}
