/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 28.11.21 
*дата останньої зміни 28.11.21 15:07
*Лабораторна №8
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою С\С++ 
для обробки елементів в рядках чи у стовпчиках матриці відповідно індивідуального завдання, 
що надане у таблиці І.2 з додатку І. Варіант обирати зменшивши на 5.
*варіант : №9 (5)
*/
#include <math.h>
#include <iostream>	
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j , n,m , min;
	srand(time(NULL));
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a4[n][m];
	int *m_p;
	m_p=&a4[0][0];
	printf(" Матриця а4( %d, %d): \n",n,m);
	for (i = 0; i < n; i++){
			for (j = 0; j < m; j++){
				*m_p = rand()%100-50;
				printf("%4d", *m_p);
				m_p++;
			}
		cout<<endl;
	}
	m_p=&a4[0][0];
	min = *m_p;
	for (i = 0; i < n; i++){
			for (j = 0; j < m; j++){
				if(*m_p <min){
					min = *m_p;
				}
				m_p++;
			}
	}
	printf(" Мінімум матриці а4 : %d \n",min);
	cout<<endl;
	system("pause");

	return 0;
}
