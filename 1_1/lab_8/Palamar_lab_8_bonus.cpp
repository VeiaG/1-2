#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j, n, m, k, e;
	srand(time(NULL));
	cout<<" Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<" Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int mat[n][m];
	int *m_p;
	double r[m];
	double *r_p;
	m_p=&mat[0][0];
	printf(" Матриця mat( %d, %d): \n",n,m);
		for (i = 0; i < n; i++){
			for (j = 0; j < m; j++){
				*m_p = rand()%100-50;
				printf("%4d", *m_p);
				m_p++;
			}
			cout<<endl;
		}
	r_p=&r[0];
	m_p=&mat[0][0];
		for (j = 0; j < m; j++)
		{
			*r_p=0;
			for (i = 0; i < n; i++){
				e=*(m_p+i*m+j);
				if (e<0){
					*r_p = *r_p + e;
					k++;
				}
			}
			if (0<k) {
				*r_p=*r_p;
			}
			r_p++;
		}
	r_p=&r[0];
	printf(" Сума усіх від'ємних значеннь кожного рядка: \n",n,m);
	for (j = 0; j < m; j++){
		cout<<" "<<*r_p;
		r_p++;
		}
	cout<<endl;
	system("pause");
	return 0;
}

