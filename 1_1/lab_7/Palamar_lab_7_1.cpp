/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 18.11.21 
*дата останньої зміни 18.11.21 
*Лабораторна №7
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для обробки усіх елементів матриці відповідно індивідуального завдання
*призначення програмнного файлу: Обчислити та вивести середнє арифметичне елементів масиву a33(n,m)
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j , n,m , num = 0 , sum = 0;
	srand(time(NULL));
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a33[n][m];
	printf("Матриця a33( %d, %d): \n",n,m);
	for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
			a33[i][j] = rand()%100-50;
			printf("%4d", a33[i][j]);
			}
		cout<<endl;
		}
	for (i = 0; i < n; i++){
		for (j = 0; j < m; j++){
			sum+=a33[i][j];
			num++;
		}
	}
	cout<<endl<<" Середнє арифметичне елементів массиву = "<<sum/num<<endl;
	system("pause");

	return 0;
}
