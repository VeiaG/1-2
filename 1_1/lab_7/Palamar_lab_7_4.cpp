/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 18.11.21 
*дата останньої зміни 18.11.21 
*Лабораторна №7
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою С\С++
модифікації для обробки тривимірного масиву відповідно індивідуального
завдання, що знаходиться у таблиці І.4 з додатку І.
*призначення програмнного файлу: В заданий багатовимірній матриці g9(3,2,3) знайти добуток тих від‘ємних
елементів, сума індексів яких є парним числом. Вивести вихідну матрицю
та добуток від‘ємних елементів.

*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
	//Процедура переведення курсору в задану позицію на екрані консолі
void gotoxy(int xpos, int ypos)
{ COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X = xpos; scrn.Y = ypos;
	SetConsoleCursorPosition(hOuput,scrn);
}
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const int N=3, M=2, H=3, D=4;
	int i, j, k, dob = 1, g9[N][M][H];
	srand(time(NULL));
	printf("Вхідна g9 ( %d, %d, %d): \n",N,M,H);
	for (i = 0; i < N; i++)
	for (j = 0; j < M; j++)
	for (k = 0; k < H; k++)
		{
		g9[i][j][k] = rand()%100-50;
		gotoxy(((j+k)*D+1),(((1+i)*M)-j));
		printf("%d", g9[i][j][k]);
			if((i+j+k)%2 == 0 && g9[i][j][k]< 0 ){
				dob*=g9[i][j][k];
			}
		}
	gotoxy(0, N*M);
	cout<<"Добуток від'ємних значень масиву із парною сумою індексів = "<<dob<<endl;
	system("pause");

	return 0;
}
