/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 02.11.21 20:58
*дата останньої зміни 02.11.21 21:46
*Лабораторна №5
*завдання :Розробити блок-схему алгоритму для табулювання функції та
реалізувати його мовою С\С++ відповідно варіанту індивідуального завдання, що
наведено у таблиці Е.1 з додатку Е
*призначення програмнного файлу: табулювання заданої ф-ї
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double x, y, xstart, xend, xstep;
	cout<<" Введіть початкое значення Х = ";
	cin>>xstart;
	cout<<" Введіть кінцеве значення Х = ";
	cin>>xend;
	cout<<" Введіть значення зміни X = ";
	cin>>xstep;
	printf("_________________________\n");
	printf("| x | y |\n");
	printf("-------------------------\n");
	x=xstart;
		do
		{
			if (pow((x+2),2) > 5 ){
				y=cos(x-1/exp(2));
			}
			else{
				y=pow((x+1),1/3)+tan(pow(x,2));
			}
		printf("| %8.2f | %8.4f |\n", x, y);
		x=x+xstep;
		}
		while (x<=xend);
	printf("-------------------------\n");
	system("pause");
	return 0;
}
