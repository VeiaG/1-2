/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 02.11.21 20:58
*дата останньої зміни 02.11.21 21:34
*Лабораторна №5
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для підрахунку сум та добутків, відповідно індивідуального завдання
*призначення програмнного файлу: розрахунок значення за заданою формулою
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#define M_PI 3.14159265358979323846
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double sum, mul, Z;
	int i, k,a, i_start, i_end, k_start, k_end;
	cout<<" Введіть початкові значення i = ";
	cin>>i_start;
	cout<<" Введіть кінцеве значення i = ";
	cin>>i_end;
	cout<<" Введіть початкові значення k = ";
	cin>>k_start;
	cout<<" Введіть кінцеве значення k = ";
	cin>>k_end;
	cout<<" Введіть a = ";
	cin>>a;
	sum=0;
	i=i_start-1;
		do
		{
		i++;
		sum+=pow((sin(i)+pow(cos(i),2)),3);
		}
		while (i<=i_end);
	mul=1;
	k=k_start-1;
		do
		{
			k++;
			mul*=sin((M_PI/3)+k)/(2*k);
		}
		while (k<=k_end);
	Z=(2.151/sum)+(1/pow(5,1/a))*mul;
	cout<<" Сума = "<< sum <<endl;
	cout<<" Добуток = "<< mul <<endl;
	printf(" Z = %7.5f \n", Z);
	system("pause");
	return 0;
}
