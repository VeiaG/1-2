/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 02.11.21 20:58
*дата останньої зміни 03.11.21 00:20
*Лабораторна №5
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для підрахунку елементів нескінченного ряду
*призначення програмнного файлу:  табулювання заданої ф-ї
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double e, x, sum=0;
	int i=0;
	cout<<" Введіть e (від 0 до 1) = ";
	cin>>e;
	printf("___________________\n");
	printf("|  #  | x          |\n");
	printf("-------------------\n");
		do
		{
			i++;
			x=pow((i*(pow(i,2)+i+1)),1/5)+log(fabs(sin(i)/3));
			printf("| %3d | %10.5f |\n", i,x);
		}
		while (e<=(fabs(x)));
	printf("--------------------\n");
	system("pause");
	return 0;
}
