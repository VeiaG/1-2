/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 08.12.21 
*дата останньої зміни 08.12.21 
*Лабораторна №10
*завдання Розробити блок-схему алгоритму та реалізувати його мовою
С\С++ для обробки слова або символьного рядка відповідно індивідуального
завдання, що наведено у таблиці Л.1 додатку Л.
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string a;
	int i ,s , k=0;
	printf("Введіть рядок: \n");
	getline(cin,a);
	s=a.length();
	system("cls");
	i=0;
	while(i<s){
		if(a[i] == ' ' && a[i+1] == ' '){
			a.erase(i,1);
			i=0;
			k++;
		}
		else
		i++;
	}
	for(i=0; i<k ; i++)
	{
		a.append(" ");
	}
	printf("Редагований рядок : \n");
	SetColor(0,2);
	cout<<a<<endl;
	SetColor(7,0);
	system("pause");

	return 0;
}
