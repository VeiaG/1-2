/*file name: lab_1_1.cpp
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення 10.09.2021 11:02
*дата останньої зміни 10.09.2021 18:41
*Лабораторна №1
*завдання :Розробити блок-схеми алгоритму для обчислення розв'язку задачі, та реалізувати його мовою C++ відповідно варіанту індивідуального завдання.
*призначення програмнного файлу: Розрахування висоти циліндра за його об'ємом та площою основи
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	system("color 55");
	double V, S, h;
	SetColor(4,0);
	cout<<"Введіть об'єм циліндра = ";
	cin>>V;
	SetColor(12,0);
	cout<<"Введіть площу основи циліндра = ";
	cin>>S;
	h=V/S;
	SetColor(10,0);
	printf("Висота циліндра= %5.3f\n",h);
	SetColor(7,0);
	system("pause");	
	return 0;
}
