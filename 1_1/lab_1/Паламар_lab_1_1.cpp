/*file name: lab_1_1.cpp
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 10.09.2021 10:38
*дата останньої зміни 10.09.2021 18:40
*Лабораторна №1
*завдання :Розробити блок-схеми алгоритму для обчислення величин , що задані виразами , та реалізувати його мовою C++ відповідно варіанту індивідуального завдання.
*призначення програмнного файлу: Обчислення трьох змінних ,Z1,Z2,Z3 за даними формулами 
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	system("color 55");
	const int A=1,B=2, C=1,D=2, F=4,G=6,Z=2,I=5, R=2,T=2,V=5;
	double a,b,c,x,y;
	cout<<"Введіть значення змінної x = ";
	cin>>x;
	cout<<"Введіть значення змінної y = ";
	cin>>y;
	a=(sqrt(fabs(x-A))-sqrt(fabs(B*y)))/(C+(x*y)/D);
	b=x-((pow(x,F)/y)-(pow(x,G)/pow(y,Z)))+sqrt(fabs(y-I));
	c=(R/b)+(pow(a,T)/V);
	SetColor(10,0);
	printf("Результат Z1= %5.3f\n",a);
	SetColor(11,1	);
	printf("Результат Z2= %5.3f\n",b);
	SetColor(4,8);
	printf("Результат Z3= %5.3f\n",c);
	SetColor(7,0);
	system("pause");
	return 0;
}
