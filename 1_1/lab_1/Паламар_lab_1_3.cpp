/*file name: lab_1_1.cpp
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення 10.09.2021 11:02
*дата останньої зміни 12.09.2021 16:30
*Лабораторна №1
*завдання :Розробити програму мовою C++ дял відображення заданого зображення відповідно варіанту індивідуального завдання.
*призначення програмнного файлу: Виведення на екран (консоль) зображення паровозу
*варіант : №9 (Паровоз)
*/
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
void gotoxy(int xp, int yp){
	COORD new_xy;
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	new_xy.X =xp;
	new_xy.Y =yp;
	SetConsoleCursorPosition(hStdOut,new_xy);
}
int main(){
	
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const int t=200, p=10;
	int i = 0;
	SetColor(0,1);
	do {
	i++;
	system("cls");
	cout<<"           ** *     *                     "<<endl;
	cout<<"          *** *                           "<<endl;
	cout<<"         ***           _---------_        "<<endl;
	cout<<"         /  |          |         |        "<<endl;
	cout<<"         |  |          |         |        "<<endl;
	cout<<"         |  |          |         |        "<<endl;
	cout<<"      _--|__|----------|_________|        "<<endl;
	cout<<"     |                           |        "<<endl;
	cout<<"     |                           |        "<<endl;
	cout<<"     |_(%#%)______(%#%)___(%#%)__|        "<<endl;
	cout<<"       (%#%)      (%#%)   (%##)           "<<endl;
	cout<<"------*-------------------------*---------"<<endl;
	/*Coloring */
		gotoxy(6,7);
		SetColor(0,4);
		cout<<"                           ";
		gotoxy(6,8);
		cout<<"                           ";
		gotoxy(6,9);
		cout<<"_(%#%)______(%#%)___(%#%)__";
		
		gotoxy(7,9);
		SetColor(0,7);
		cout<<"(%#%)";
		gotoxy(18,9);
		cout<<"(%#%)";
		gotoxy(26,9);
		cout<<"(%#%)";
		
		SetColor(0,11);
		gotoxy(24,3);
		cout<<"         ";
		gotoxy(24,4);
		cout<<"         ";
		gotoxy(24,5);
		cout<<"         ";
		gotoxy(24,6);
		cout<<"_________";

		SetColor(0,12);
		gotoxy(10,3);
		cout<<"  ";
		gotoxy(10,4);
		cout<<"  ";
		gotoxy(10,5);
		cout<<"  ";
		gotoxy(10,6);
		cout<<"__";
		
		SetColor(0,2);
		gotoxy(7,9);
		cout<<"(%#%)";
		gotoxy(18,9);
		cout<<"(%#%)";
		gotoxy(26,9);
		cout<<"(%#%)";
		
		gotoxy(7,10);
		cout<<"(%#%)";
		gotoxy(18,10);
		cout<<"(%#%)";
		gotoxy(26,10);
		cout<<"(%#%)";
		
		gotoxy(0,0);
		SetColor(7,1);
		cout<<"           ** *     *                     "<<endl;
		cout<<"          *** *                           "<<endl;
		cout<<"         ***           "<<endl;		
		
		gotoxy(0,0);
		SetColor(0,1);
	Sleep(t);
	system("cls");
	cout<<"               * *  *       *             "<<endl;
	cout<<"            * * *                         "<<endl;
	cout<<"         **  *         _---------_        "<<endl;
	cout<<"         /  |          |         |        "<<endl;
	cout<<"         |  |          |         |        "<<endl;
	cout<<"         |  |          |         |        "<<endl;
	cout<<"      _--|__|----------|_________|        "<<endl;
	cout<<"     |                           |        "<<endl;
	cout<<"     |                           |        "<<endl;
	cout<<"     |_(#%%)______(#%%)___(#%%)__|        "<<endl;
	cout<<"       (%%#)      (%%#)   (%%#)           "<<endl;
	cout<<"-------------*-------------------------*--"<<endl;
	/*Coloring */
		gotoxy(6,7);
		SetColor(0,4);
		cout<<"                           ";
		gotoxy(6,8);
		cout<<"                           ";
		gotoxy(6,9);
		cout<<"_(#%%)______(#%%)___(#%%)__";
		
		gotoxy(7,9);
		SetColor(0,7);
		cout<<"(#%%)";
		gotoxy(18,9);
		cout<<"(#%%)";
		gotoxy(26,9);
		cout<<"(#%%)";
		
		SetColor(0,11);
		gotoxy(24,3);
		cout<<"         ";
		gotoxy(24,4);
		cout<<"         ";
		gotoxy(24,5);
		cout<<"         ";
		gotoxy(24,6);
		cout<<"_________";

		SetColor(0,12);
		gotoxy(10,3);
		cout<<"  ";
		gotoxy(10,4);
		cout<<"  ";
		gotoxy(10,5);
		cout<<"  ";
		gotoxy(10,6);
		cout<<"__";
		
		SetColor(0,2);
		gotoxy(7,9);
		cout<<"(#%%)";
		gotoxy(18,9);
		cout<<"(#%%)";
		gotoxy(26,9);
		cout<<"(#%%)";
		
		gotoxy(7,10);
		cout<<"(%%#)";
		gotoxy(18,10);
		cout<<"(%%#)";
		gotoxy(26,10);
		cout<<"(%%#)";
		
		gotoxy(0,0);
		SetColor(7,1);
		cout<<"               * *  *       *             "<<endl;
		cout<<"            * * *                         "<<endl;
		cout<<"         **  *      "<<endl;
		
		gotoxy(0,0);
		SetColor(0,1);
	Sleep(t);
	system("cls");
	cout<<"             *     *     *  **            "<<endl;
	cout<<"           *  *       *   **              "<<endl;
	cout<<"          **           _---------_        "<<endl;
	cout<<"         /  |          |         |        "<<endl;
	cout<<"         |  |          |         |        "<<endl;
	cout<<"         |  |          |         |        "<<endl;
	cout<<"      _--|__|----------|_________|        "<<endl;
	cout<<"     |                           |        "<<endl;
	cout<<"     |                           |        "<<endl;
	cout<<"     |_(%%#)______(%%#)___(%%#)__|        "<<endl;
	cout<<"       (#%%)      (#%%)   (#%%)           "<<endl;
	cout<<"--*------------------*--------------------"<<endl;
	/*Coloring */
		gotoxy(6,7);
		SetColor(0,4);
		cout<<"                           ";
		gotoxy(6,8);
		cout<<"                           ";
		gotoxy(6,9);
		cout<<"_(%%#)______(%%#)___(%%#)__";
		
		gotoxy(7,9);
		SetColor(0,7);
		cout<<"(%%#)";
		gotoxy(18,9);
		cout<<"(%%#)";
		gotoxy(26,9);
		cout<<"(%%#)";
		
		SetColor(0,11);
		gotoxy(24,3);
		cout<<"         ";
		gotoxy(24,4);
		cout<<"         ";
		gotoxy(24,5);
		cout<<"         ";
		gotoxy(24,6);
		cout<<"_________";

		SetColor(0,12);
		gotoxy(10,3);
		cout<<"  ";
		gotoxy(10,4);
		cout<<"  ";
		gotoxy(10,5);
		cout<<"  ";
		gotoxy(10,6);
		cout<<"__";
		
		SetColor(0,2);
		gotoxy(7,9);
		cout<<"(%%#)";
		gotoxy(18,9);
		cout<<"(%%#)";
		gotoxy(26,9);
		cout<<"(%%#)";
		
		gotoxy(7,10);
		cout<<"(#%%)";
		gotoxy(18,10);
		cout<<"(#%%)";
		gotoxy(26,10);
		cout<<"(#%%)";
		
		gotoxy(0,0);
		SetColor(7,1);
		cout<<"             *     *     *  **            "<<endl;
		cout<<"           *  *       *   **              "<<endl;
		cout<<"          **        "<<endl;
		
		gotoxy(0,0);
		SetColor(0,1);
	Sleep(t);
	} while (i<p);
	gotoxy(0,15);
	system("pause");
	return 0;
}
