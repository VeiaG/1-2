/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 23.09.21 14:46
*дата останньої зміни 23.09.21 16:18
*Лабораторна №1
*завдання : Розробити блок-схему алгоритму для обчислення індивідуальних завдань та реалізувати його мовою С++
*призначення програмнного файлу: Виведення на екран назви введеної цифри на англійській
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int a;
	cout<<"Введіть цифру від 0 до 9 = ";
	cin>>a;
	switch(a){
		case 0 :{
			SetColor(a,7);
			cout<<"Zero"<<endl;
			break;
		}
		case 1 :{
			SetColor(a,0);
			cout<<"One"<<endl;
			break;
		}
		case 2 :{
			SetColor(a,0);
			cout<<"Two"<<endl;
			break;
		}
		case 3 :{
			SetColor(a,0);
			cout<<"Three"<<endl;
			break;
		}
		case 4 :{
			SetColor(a,0);
			cout<<"Four"<<endl;
			break;
		}
		case 5 :{
			SetColor(a,0);
			cout<<"Five"<<endl;
			break;
		}
		case 6 :{
			SetColor(a,0);
			cout<<"Six"<<endl;
			break;
		}
		case 7 :{
			SetColor(a,0);
			cout<<"Seven"<<endl;
			break;
		}
		case 8 :{
			SetColor(a,0);
			cout<<"Eight"<<endl;
			break;
		}
		case 9 :{
			SetColor(a,0);
			cout<<"Nine"<<endl;
			break;
		}
		default :{
			SetColor(2,0);
			cout<<"Число не входить в заданий діапазон"<<endl;
			break;
		}
	}
	SetColor(7,0);
	system("pause");
	return 0;
}
