/*file name: 
*студент: Паламар Роман Володимирович 
*група : КН-1-2
*дата створення: 23.09.21 14:46
*дата останньої зміни 04.10.2021 22:03
*Лабораторна №1
*завдання :Розробити блок-схему алгоритму для розв‘язання індивідуальних завдань та реалізувати його мовою C++
*призначення програмнного файлу: Визначити , чи серед трьох цілих двохзначних числел є хоча б одна пара , яка не співпадає
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
//Функція Reverse перевертає задане число ( ну типу з 123 стане 321 )
int Reverse(int x)
{
   int y = 0;
   while(x)
   {
       y = y*10 + x%10;
       x /= 10;
   }
   return y;
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double a,b,c,rb,rc;
	SetColor(10,0);
	cout<<"Введіть перше двохзначне число = ";
	cin>>a;
	SetColor(11,0);
	cout<<"Введіть друге двохзначне число = ";
	cin>>b;
	SetColor(6,0);
	cout<<"Введіть Третє двохзначне число = ";
	cin>>c;
	rb=Reverse(b);
	rc=Reverse(c);
	if ((a == rb || a == b) && (a == rc || a == c) && ( b == rc || b == c ) ){
		SetColor(4,0);
		cout<<"Пари співпадають"<<endl;
	}
	else
	{
		SetColor(2,0);
		cout<<"Пари не співпадають"<<endl;
	}
	SetColor(7,0);
	system("pause");
	return 0;
}
