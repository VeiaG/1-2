/*file name: Palamar_lab_2_1.cpp
	*студент: Паламар Роман Володимирович 
	*група : КН-1-2
*дата створення: 23.09.21 14:45
*дата останньої зміни 23.09.21 15:09
*Лабораторна №2
*завдання : Розробити блок-схему алгоритму для обчислення величини, яка
задана системою виразів та реалізувати його мовою С++ відповідно варіанту
індивідуального завдання
*призначення програмнного файлу: Обчислення змінної j за заданими правилами
*варіант : №9
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const double d=10;
	double f,g,fg,j;
	SetColor(4,0);
	cout<<"Введіть змінну f = ";
	cin>>f;
	SetColor(11,0);
	cout<<"Введіть змінну g = ";
	cin>>g;
	fg=fabs(f*g);
	if (fg>d)
		j=log(fabs(f)+(fabs(g)));
	else 
		if (fg<d)
			j=exp(f+g);
		else 
			j=f+g;
	SetColor(2,0);
	cout<<endl;
	printf("Результат j=  %5.3f\n",j);
	SetColor(7,0);
	system("pause");
	return 0;
}
